package com.chb.test;

import com.chb.utils.ESClient;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.aggregations.metrics.ExtendedStats;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;

import java.io.IOException;


public class AggDemo {
    RestHighLevelClient client = ESClient.getClient();
    String index = "sms-logs-index";


    @Test
    public void extended_stats() throws IOException {
        //1.SearchRequest
        SearchRequest request=new SearchRequest(index);

        //2.聚合查询条件
        SearchSourceBuilder builder=new SearchSourceBuilder();
        builder.aggregation(AggregationBuilders.extendedStats("agg").field("fee"));
        request.source(builder);

        //3.执行查询
        SearchResponse resp = client.search(request, RequestOptions.DEFAULT);

        //4.返回结果，默认返回的是父类Aggregation
        ExtendedStats agg=resp.getAggregations().get("agg");

        double max = agg.getMax();
        double min = agg.getMin();

        //其他的属性就不一一例举了

        System.out.println("max fee :  "+max+"  min fee :  "+min);
    }


    @Test
    public void range() throws IOException {
        //1.SearchRequest
        SearchRequest request=new SearchRequest(index);

        //2.聚合查询条件
        SearchSourceBuilder builder=new SearchSourceBuilder();
        builder.aggregation(AggregationBuilders.range("agg").field("fee").addUnboundedTo(200).addRange(200,400).addUnboundedFrom(400));
        request.source(builder);

        //3.执行查询
        SearchResponse resp = client.search(request, RequestOptions.DEFAULT);

        //4.返回结果，默认返回的是父类Aggregation
        Range agg = resp.getAggregations().get("agg");
        for (Range.Bucket bucket : agg.getBuckets()) {
            String key=bucket.getKeyAsString();
            Object from = bucket.getFrom();
            Object to = bucket.getTo();
            long docCount = bucket.getDocCount();

            System.out.println(String.format("key：%s，from：%s，to，%s，docCount：%s",key,from,to,docCount));
        }
    }


    @Test
    public void cardnality() throws IOException {
        //1.SearchRequest
        SearchRequest request = new SearchRequest(index);

        //2.聚合查询条件
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.aggregation(AggregationBuilders.cardinality("agg").field("province"));
        request.source(builder);

        //3.执行查询
        SearchResponse resp = client.search(request, RequestOptions.DEFAULT);

        //4.返回结果，默认返回的是父类Aggregation
        Aggregation agg = resp.getAggregations().get("agg");

        //转换成子接口Cardinality才有getValue()方法，父接口Aggregation没有该方法
        Cardinality myagg = (Cardinality) agg;

        //以上两句代码也可以写成直接写成    Cardinality agg = resp.getAggregations().get("agg");
        //后续所有的聚合查询都要有此类似的操作，切记！

        long value = myagg.getValue();
        System.out.println(value);
    }
}
