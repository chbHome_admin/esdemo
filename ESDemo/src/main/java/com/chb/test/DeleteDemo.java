package com.chb.test;

import com.chb.utils.ESClient;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.junit.Test;

import java.io.IOException;

public class DeleteDemo {
    RestHighLevelClient client = ESClient.getClient();
    String index = "person";

    @Test
    public void deleteByQuery() throws IOException {
        // 1、创建DeleteByQueryRequest对象
        DeleteByQueryRequest request = new DeleteByQueryRequest(index);

        // 2、指定检索条件, 和 SearchRequest指定query方式不一样
        request.setQuery(QueryBuilders.rangeQuery("age").gte(30).lte(50));

        // 3、 执行删除
        BulkByScrollResponse resp = client.deleteByQuery(request, RequestOptions.DEFAULT);

        // 4、返回结果
        System.out.println(resp);
    }
}
