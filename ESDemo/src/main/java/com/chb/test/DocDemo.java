package com.chb.test;

import com.chb.bean.Person;
import com.chb.utils.ESClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DocDemo {
    String index = "person";
    RestHighLevelClient client = ESClient.getClient();

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void bulkDeleteDoc() throws IOException {
        // DeleteRequest request = new DeleteRequest(index, "N67mgXgB_tiW03WV73UZ");
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.add(new DeleteRequest(index, "3"));
        bulkRequest.add(new DeleteRequest(index, "4"));
        bulkRequest.add(new DeleteRequest(index, "5"));

        client.bulk(bulkRequest, RequestOptions.DEFAULT);
    }
    @Test
    public void bulkCreateDoc() throws IOException {
        // 1、准备json数据
        Person person1 = new Person(3, "张三", 33, new Date());
        Person person2 = new Person(4, "李四", 44, new Date());
        Person person3 = new Person(5, "王五", 55, new Date());
        String json1 = mapper.writeValueAsString(person1);
        String json2 = mapper.writeValueAsString(person2);
        String json3 = mapper.writeValueAsString(person3);

        // 准备request对象用于添加数据
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.add(new IndexRequest(index).id(person1.getId().toString()).source(json1, XContentType.JSON));
        bulkRequest.add(new IndexRequest(index).id(person2.getId().toString()).source(json2, XContentType.JSON));
        bulkRequest.add(new IndexRequest(index).id(person3.getId().toString()).source(json3, XContentType.JSON));

        // 3、通过client对象执行, 注意此处与操作Index的区别 client.indices().create
        BulkResponse resp = client.bulk(bulkRequest, RequestOptions.DEFAULT);

        // 4、打印结果
        System.out.println(resp);
    }


    @Test
    public void createDoc() throws IOException {
        // 1、准备json数据
        Person person = new Person(3, "zhaosan", 33, new Date());
        String json = mapper.writeValueAsString(person);

        // 准备request对象用于添加数据
        IndexRequest request = new IndexRequest(index);
        request.source(json, XContentType.JSON); // 添加数据

        // 3、通过client对象执行, 注意此处与操作Index的区别 client.indices().create
        IndexResponse resp = client.index(request, RequestOptions.DEFAULT);

        // 4、打印结果
        System.out.println(resp);
    }


    @Test
    public void updateDoc() throws IOException {
        // 1、创建一个Map, 指定需要修改的内容
        Map<String, Object> doc = new HashMap<String, Object>();
        doc.put("name", "张三");
        String docId = "N67mgXgB_tiW03WV73UZ";

        // 2、创建request对象，封装数据
        UpdateRequest updateRequest = new UpdateRequest(index, docId);
        updateRequest.doc(doc);

        // 3、执行
        UpdateResponse resp = client.update(updateRequest, RequestOptions.DEFAULT);

        // 结果
        System.out.println(resp.getResult().toString());
    }

    @Test
    public void deleteDoc() throws IOException {
        DeleteRequest request = new DeleteRequest(index, "N67mgXgB_tiW03WV73UZ");

        client.delete(request, RequestOptions.DEFAULT);
    }
}
