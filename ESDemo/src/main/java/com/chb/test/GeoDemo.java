package com.chb.test;

import com.chb.utils.ESClient;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GeoDemo {
    RestHighLevelClient client = ESClient.getClient();
    String index = "map";

    @Test
    public void geoDistance() throws IOException {
        // 1、SearchRequest对象
        SearchRequest searchRequest = new SearchRequest(index);

        // 2、指定查询参数
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.geoDistanceQuery("location").distance("3000").point(39.908404,116.433733));
        // 将条件添加到request对象
        searchRequest.source(searchSourceBuilder);

        // 3、执行
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 4、输出
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

    }


    @Test
    public void geoBoundingBox() throws IOException {
        // 1、SearchRequest对象
        SearchRequest searchRequest = new SearchRequest(index);

        // 2、指定查询参数
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // =======改变的代码========
        searchSourceBuilder.query(QueryBuilders.geoBoundingBoxQuery("location").setCorners(39.95499,116.326943,39.908737,116.433446));
        // =====================
        // 将条件添加到request对象
        searchRequest.source(searchSourceBuilder);

        // 3、执行
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 4、输出
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

    }


    @Test
    public void geoPolygon() throws IOException {
        // 1、SearchRequest对象
        SearchRequest searchRequest = new SearchRequest(index);

        // 2、指定查询参数
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // =======改变的代码========
        List<GeoPoint> points=new ArrayList<GeoPoint>();
        //注意java里面是纬经度的顺序，不要搞反了
        points.add(new GeoPoint(39.976004,116.29561));
        points.add(new GeoPoint(39.996348,116.364528));
        points.add(new GeoPoint(40.003423,116.300209));
        searchSourceBuilder.query(QueryBuilders.geoPolygonQuery("location", points));
        // =====================
        // 将条件添加到request对象
        searchRequest.source(searchSourceBuilder);

        // 3、执行
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 4、输出
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

    }


}
