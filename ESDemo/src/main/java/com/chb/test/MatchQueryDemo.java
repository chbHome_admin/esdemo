package com.chb.test;

import com.chb.utils.ESClient;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class MatchQueryDemo {
    RestHighLevelClient client = ESClient.getClient();
    //定义索引名
    String index = "sms-logs-index";

    @Test
    public void matchAllQuery() throws IOException {
        //1、创建SearchRequest对象
        SearchRequest searchRequest = new SearchRequest();

        //2、指定查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(20); // ES 默认只查询10条，如果想查询更多，添加size()
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        // 3、将条件封装到Request对象中
        searchRequest.source(searchSourceBuilder);

        // 4、执行查询
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 5、打印结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

        // 打印条数
        System.out.println(resp.getHits().getHits().length);

    }

    @Test
    public void matchQuery() throws IOException {
        //1、创建SearchRequest对象
        SearchRequest searchRequest = new SearchRequest();

        //2、指定查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(20); // ES 默认只查询10条，如果想查询更多，添加size()

        // searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.query(QueryBuilders.matchQuery("smsContent", "魅力宣传"));

        // 3、将条件封装到Request对象中
        searchRequest.source(searchSourceBuilder);

        // 4、执行查询
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 5、打印结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

        // 打印条数
        System.out.println(resp.getHits().getHits().length);

    }

    @Test
    public void matchBoolQuery() throws IOException {
        //1、创建SearchRequest对象
        SearchRequest searchRequest = new SearchRequest();

        //2、指定查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(20); // ES 默认只查询10条，如果想查询更多，添加size()

        // searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.query(QueryBuilders.matchQuery("smsContent", "魅力 宣传").operator(Operator.AND));

        // 3、将条件封装到Request对象中
        searchRequest.source(searchSourceBuilder);

        // 4、执行查询
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 5、打印结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

        // 打印条数
        System.out.println(resp.getHits().getHits().length);

    }


    @Test
    public void multiMatchQuery() throws IOException {
        //1、创建SearchRequest对象
        SearchRequest searchRequest = new SearchRequest();

        //2、指定查询条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.size(20); // ES 默认只查询10条，如果想查询更多，添加size()

        // searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.query(QueryBuilders.multiMatchQuery("北京", "province", "smsContent"));

        // 3、将条件封装到Request对象中
        searchRequest.source(searchSourceBuilder);

        // 4、执行查询
        SearchResponse resp = client.search(searchRequest, RequestOptions.DEFAULT);

        // 5、打印结果
        for (SearchHit hit : resp.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }

        // 打印条数
        System.out.println(resp.getHits().getHits().length);

    }
}
