package com.chb.test;

import com.chb.utils.ESClient;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class TermLevelQueryDemo {
    RestHighLevelClient client = ESClient.getClient();
    //定义索引名
    String index = "sms-logs-index";

    @Test
    public void regexpQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();

        // 查询套路， 只需要修改此处
        builder.from(0).size(10).query(QueryBuilders.regexpQuery("moblie", "13[0-9]{9}"));

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }

    @Test
    public void rangeQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();

        // 查询套路， 只需要修改此处
        builder.from(0).size(10).query(QueryBuilders.rangeQuery("fee").gte(100).lte(500));

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }

    @Test
    public void wildcardQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();

        // 查询套路， 只需要修改此处
        builder.from(0).size(10).query(QueryBuilders.wildcardQuery("corpName", "*集团"));

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }

    @Test
    public void fuzzyQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();

        // 查询套路， 只需要修改此处
        builder.from(0).size(10).query(QueryBuilders.fuzzyQuery("corpName", "中文集团").fuzziness(Fuzziness.TWO));

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }



    @Test
    public void prefixQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();

        // 查询套路， 只需要修改此处
        builder.from(0).size(10).query(QueryBuilders.prefixQuery("corpName", "上海"));

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }


}
