package com.chb.test;

import com.chb.utils.ESClient;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class TermQueryDemo {
    RestHighLevelClient client = ESClient.getClient();
    //定义索引名
    String index = "sms-logs-index";

    @Test
    public void termQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.from(0).size(10).query(QueryBuilders.termQuery("province", "北京")); //指定term查新条件

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }


    @Test
    public void termsQuery() throws IOException {
        //1。创建request对象，查询用的对象一般都是SearchRequest对象
        SearchRequest mySearchRequest = new SearchRequest(index);

        //2，指定查询条件，依赖查询条件的对象SearchSourceBuilder的对象
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.from(0).size(10).query(QueryBuilders.termsQuery("province", "北京", "上海", "杭州")); //指定term查新条件

        mySearchRequest.source(builder);
        //3. 执行查询
        SearchResponse search = client.search(mySearchRequest, RequestOptions.DEFAULT);

        //4. 获取到_source中的数据，并展示
        //注意RESTFUL风格上是两个hits，所以这里要两次getHits()
        for (SearchHit hit : search.getHits().getHits()) {
            Map<String, Object> result = hit.getSourceAsMap();
            System.out.println(result);
        }
    }

}
