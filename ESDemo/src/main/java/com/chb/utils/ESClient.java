package com.chb.utils;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;

public class ESClient {
    public static RestHighLevelClient getClient() {
        // 创建HttpHost
        HttpHost httpHost = new HttpHost("s203", 9200);

        // 创建 RestClientBuilder
        RestClientBuilder restClientBuilder = RestClient.builder(httpHost);

        // 创建RestHighLevelClient
        RestHighLevelClient client = new RestHighLevelClient(restClientBuilder);

        return client;
    }
}
